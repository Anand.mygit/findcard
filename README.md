# Find Card (XCode project)

This is a project done as per requirement mentioned in document.
- Used MVVM architecture 
- Unit test included
- UI test included

Xcode version used 
```
Version 11.1 (11A1027)
```

Deployment target
```
iOS 13.1
```

Number Of pairs 
```
Change at AppConfig.numberOfPairs = 6
```
Number Of pairs can be changed in AppConfig file by updating this value

Code coverage
```
94.1%
```