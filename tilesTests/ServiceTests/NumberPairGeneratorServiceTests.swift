//
//  NumberPairGeneratorServiceTests.swift
//  tilesTests
//
//  Created by Anand, Chetan on 25/4/20.
//  Copyright © 2020 bigpay. All rights reserved.
//

import Foundation
@testable import tiles
import XCTest

class NumberPairGeneratorServiceTests: XCTestCase {
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_whenGeneratePairsCalled_shouldCreateNumberPairArray() {
        let service = NumberPairGeneratorService(numberOfPairs: 6)
        service.generatePairs()

        XCTAssertTrue(service.shuffledNumberPairs.count > 0)
    }
}
