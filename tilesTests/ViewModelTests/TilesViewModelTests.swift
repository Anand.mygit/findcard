//
//  TilesViewModelTests.swift
//  tilesTests
//
//  Created by Anand, Chetan on 25/4/20.
//  Copyright © 2020 Chetan. All rights reserved.
//

@testable import tiles
import XCTest

class TilesViewModelTests: XCTestCase {
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_whenGenerateTilesCalled_shouldUpdateItems() {
        let service = NumberPairGeneratorService(numberOfPairs: 6)
        service.generatePairs()
        let viewModel = TilesViewModel(service: service)
        viewModel.generateTiles()

        XCTAssertTrue(viewModel.items.value.count == 12)
    }

    func test_whenValidatePairsCalledWithoutPairsFlipped_shouldReturnFailure() {
        let promise = expectation(description: "No Matching pairs \(#function)\(#line)")

        let service = NumberPairGeneratorService(numberOfPairs: 6)
        service.generatePairs()
        let viewModel = TilesViewModel(service: service)
        viewModel.generateTiles()

        let tileViewModel = viewModel.items.value.first!
        viewModel.validate(tile: tileViewModel, afterDelay: 1) { success, _ in
            XCTAssertFalse(success)
            promise.fulfill()
        }

        waitForExpectations(timeout: 10, handler: nil)
    }

    func test_whenValidatePairsCalledWithPairsFlipped_shouldReturnSuccess() {
        let promise = expectation(description: "No Matching pairs \(#function)\(#line)")

        let service = NumberPairGeneratorService(numberOfPairs: 6)
        service.generatePairs()
        let viewModel = TilesViewModel(service: service)
        viewModel.generateTiles()

        let anyTileViewModel = viewModel.items.value.first!

        let pair = viewModel.items.value.filter { $0.value == anyTileViewModel.value }
        if let firstMatch = pair.first,
            let secondMatch = pair.last {
            firstMatch.flipped = true
            secondMatch.flipped = true
        }

        viewModel.validate(tile: anyTileViewModel, afterDelay: 1) { success, _ in
            XCTAssertTrue(success)
            promise.fulfill()
        }

        waitForExpectations(timeout: 10, handler: nil)
    }

    func test_whenCheckForGameOverWithUnResolvedTiles_shouldReturnFailure() {
        let service = NumberPairGeneratorService(numberOfPairs: 6)
        service.generatePairs()
        let viewModel = TilesViewModel(service: service)
        viewModel.generateTiles()

        let anyTileViewModel = viewModel.items.value.first!

        let pair = viewModel.items.value.filter { $0.value == anyTileViewModel.value }
        if let firstMatch = pair.first,
            let secondMatch = pair.last {
            firstMatch.isResolved = true
            secondMatch.isResolved = false
        }

        let isGameOver = viewModel.checkForGameover()
        XCTAssertFalse(isGameOver)
    }

    func test_whenCheckForGameOverWithAllResolvedTiles_shouldReturnSuccess() {
        let service = NumberPairGeneratorService(numberOfPairs: 6)
        service.generatePairs()
        let viewModel = TilesViewModel(service: service)
        viewModel.generateTiles()

        viewModel.items.value.forEach { tile in
            tile.isResolved = true
        }

        let isGameOver = viewModel.checkForGameover()
        XCTAssertTrue(isGameOver)
    }

    func test_whenReset_shouldRecreateItems() {
        let service = NumberPairGeneratorService(numberOfPairs: 6)
        service.generatePairs()
        let viewModel = TilesViewModel(service: service)
        viewModel.generateTiles()

        viewModel.items.value.forEach { tile in
            tile.isResolved = true
        }
        viewModel.reset()
        let anyTileViewModelAfter = viewModel.items.value.first!

        XCTAssertTrue(anyTileViewModelAfter.isResolved == false)
    }
}
