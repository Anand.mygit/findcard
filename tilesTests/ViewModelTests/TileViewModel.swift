//
//  TileViewModel.swift
//  tilesTests
//
//  Created by Anand, Chetan on 25/4/20.
//  Copyright © 2020 Chetan. All rights reserved.
//

@testable import tiles
import XCTest

class TileViewModelTests: XCTestCase {
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_whenInitiateTileViewModel_shouldHaveObjectWithFalseIsResolvedProperty() {
        let tile = Tile(id: 1, value: 10)
        let viewModel = TileViewModel(tile: tile)

        XCTAssertTrue(viewModel.isResolved == false)
    }
}
