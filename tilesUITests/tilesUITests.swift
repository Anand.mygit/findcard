//
//  tilesUITests.swift
//  tilesUITests
//
//  Created by Anand, Chetan on 24/4/20.
//  Copyright © 2020 Chetan. All rights reserved.
//

import XCTest

class tilesUITests: XCTestCase {
    override func setUp() {
        continueAfterFailure = false
        let app = XCUIApplication()
        app.launchArguments = ["UITestMode"]
        // launchArguments is set to delay validation time as its not possible to automate finding pairs as humans can do by memorizing positions
        app.launch()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_whenTapOnAllPairs_shouldShowAlert() {
        let app = XCUIApplication()
        var index = 1
        while true {
            let button = app.buttons.element(boundBy: index)
            if button.label == "?" {
                button.tap()
            }
            index += 1
            if index > app.buttons.allElementsBoundByIndex.count - 1 {
                index = 1
            }
            let elementsQuery = app.alerts.element(boundBy: 0).scrollViews.otherElements.buttons.element(boundBy: 0)
            if elementsQuery.waitForExistence(timeout: 0.1) {
                elementsQuery.tap()
                XCTAssertTrue(true)
                break
            }
        }
    }

    func testLaunchPerformance() {
//        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
//            // This measures how long it takes to launch your application.
//            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
//                XCUIApplication().launch()
//            }
//        }
    }
}
