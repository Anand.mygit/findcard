//
//  AppDelegate.swift
//  tiles
//
//  Created by Anand, Chetan on 24/4/20.
//  Copyright © 2020 Chetan. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_: UIApplication, didFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        displayAppScreen()
        return true
    }
}

extension AppDelegate {
    func displayAppScreen() {
        window = UIWindow(frame: UIScreen.main.bounds)
        let startViewController = ViewController()
        window?.rootViewController = startViewController
        window?.makeKeyAndVisible()
    }
}
