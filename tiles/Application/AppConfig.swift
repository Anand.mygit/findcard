//
//  AppConfig.swift
//  tiles
//
//  Created by Anand, Chetan on 25/4/20.
//  Copyright © 2020 bigpay. All rights reserved.
//

import Foundation

enum AppConfig {
    static let numberOfPairs = 6
}
