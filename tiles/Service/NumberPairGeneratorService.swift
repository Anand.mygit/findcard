//
//  NumberPairGeneratorService.swift
//  tiles
//
//  Created by Anand, Chetan on 24/4/20.
//  Copyright © 2020 Chetan. All rights reserved.
//

import Foundation

class NumberPairGeneratorService {
    private var numberOfPairs = AppConfig.numberOfPairs

    init(numberOfPairs: Int = AppConfig.numberOfPairs) {
        self.numberOfPairs = numberOfPairs
    }

    private var numbers = [Int]()
    lazy var shuffledNumberPairs: [Int] = {
        numbers.shuffled()
    }()

    func generatePairs() {
        numbers.removeAll()
        (0 ..< numberOfPairs).forEach { _ in
            let randomInt = Int.random(in: 0 ..< 100)
            numbers.append(randomInt)
            numbers.append(randomInt) // appending twice to create pairs in same loop
        }
    }
}
