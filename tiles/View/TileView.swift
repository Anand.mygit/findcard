//
//  TileView.swift
//  tiles
//
//  Created by Anand, Chetan on 24/4/20.
//  Copyright © 2020 Chetan. All rights reserved.
//

import UIKit

class TileView: UIButton {
    var id: Int?
    var value: Int?
    var flipped = false
    private let flipDuration: TimeInterval = 0.5
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }

    func initView() {
        titleLabel?.numberOfLines = 0
        titleLabel?.adjustsFontSizeToFitWidth = true
        backgroundColor = .systemBlue
        setTitleColor(.darkGray, for: .highlighted)
        layer.cornerRadius = 5
        clipsToBounds = true
        setTitle("?", for: .normal)
    }

    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func flip() {
        let transitionOptions: UIView.AnimationOptions = [.transitionFlipFromLeft]

        UIView.transition(with: self, duration: flipDuration, options: transitionOptions, animations: { [weak self] in
            self?.backgroundColor = .white
            self?.setTitleColor(.darkText, for: .normal)
            self?.setTitle(String(describing: self?.value ?? 0), for: .normal)
            self?.flipped = true
        })
    }

    func flipBack() {
        let transitionOptions: UIView.AnimationOptions = [.transitionFlipFromRight]

        UIView.transition(with: self, duration: flipDuration, options: transitionOptions, animations: { [weak self] in
            self?.backgroundColor = .systemBlue
            self?.setTitleColor(.white, for: .normal)
            self?.setTitle("?", for: .normal)
            self?.flipped = false
        })
    }
}
