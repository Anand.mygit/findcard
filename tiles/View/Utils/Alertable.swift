//
//  Alertable.swift
//  tiles
//
//  Created by Anand, Chetan on 25/4/20.
//  Copyright © 2020 Chetan. All rights reserved.
//

import UIKit

public protocol Alertable {}
public extension Alertable where Self: UIViewController {
    func showAlert(title: String = "", message: String, preferredStyle _: UIAlertController.Style = .alert, buttonTitle: String = "OK", buttonHandler: ((UIAlertAction) -> Swift.Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: buttonHandler))

        present(alert, animated: true, completion: nil)
    }
}
