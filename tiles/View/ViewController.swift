//
//  ViewController.swift
//  tiles
//
//  Created by Anand, Chetan on 24/4/20.
//  Copyright © 2020 Chetan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var gridStackView: GridStackView = {
        let gridView = GridStackView(rowSize: 3)
        return gridView
    }()

    var restartButton: UIButton = {
        let button = UIButton()
        button.setTitle("Reset", for: .normal)
        button.contentHorizontalAlignment = .left
        button.setTitleColor(.white, for: .normal)
        button.setTitleColor(.lightGray, for: .highlighted)
        return button
    }()

    var stepsLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .right
        return label
    }()

    lazy var viewModel: TilesViewModel = {
        let viewModel = TilesViewModel(service: NumberPairGeneratorService())
        return viewModel
    }()

    var validationDelay: TimeInterval {
        if ProcessInfo().arguments.contains("UITestMode") {
            return 5
        }
        return 1
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        bind(to: viewModel)
        viewModel.generateTiles()
    }

    private func bind(to viewModel: TilesViewModel) {
        viewModel.items.observe(on: self) { [weak self] in
            self?.showTiles(tiles: $0)
        }

        viewModel.steps.observe(on: self) { [weak self] in
            self?.stepsLabel.text = $0
        }

        viewModel.alertMessage.observe(on: self) { [weak self] in
            if !$0.isEmpty {
                self?.showGameoverMessage(message: $0)
            }
        }
    }

    func setupViews() {
        view.addSubview(gridStackView)
        view.addSubview(stepsLabel)
        view.addSubview(restartButton)
        restartButton.addTarget(self, action: #selector(resetButtonTapped), for: .touchUpInside)

        setupLayouts()
    }

    func setupLayouts() {
        stepsLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: nil, bottom: nil, trailing: view.safeAreaLayoutGuide.trailingAnchor, padding: .init(top: 20, left: 20, bottom: 20, right: 20), size: .init(width: 200, height: 20))

        restartButton.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 20, left: 20, bottom: 20, right: 20), size: .init(width: 200, height: 20))

        gridStackView.anchor(top: stepsLabel.bottomAnchor, leading: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.safeAreaLayoutGuide.trailingAnchor, padding: .init(top: 20, left: 20, bottom: 0, right: 20))

        gridStackView.layoutIfNeeded()
    }

    private func showTiles(tiles _: [TileViewModel]) {
        gridStackView.removeAllCell()
        for item in viewModel.items.value {
            let button = TileView(frame: .zero)
            button.id = item.id
            button.value = item.value
            button.flipped = item.flipped
            button.addTarget(self, action: #selector(tileTapped), for: UIControl.Event.touchUpInside)
            gridStackView.addCell(view: button)
        }
    }

    @objc func resetButtonTapped(sender _: UIButton) {
        viewModel.reset()
    }

    func showGameoverMessage(message: String) {
        showAlert(title: "Congratulations", message: message, buttonTitle: "Try another round", buttonHandler: { [weak self] _ in
            self?.viewModel.reset()
        })
    }

    @objc func tileTapped(sender: TileView) {
        guard let tileViewModel = viewModel.items.value.filter({ $0.id == sender.id }).first, !tileViewModel.flipped else {
            return
        }
        viewModel.incrementSteps()
        tileViewModel.flipped = true
        sender.flip()
        viewModel.validate(tile: tileViewModel, afterDelay: validationDelay) { success, tile in
            if !success {
                sender.flipBack()
                tile.flipped = false
            }
        }
    }
}

extension ViewController: Alertable {}
