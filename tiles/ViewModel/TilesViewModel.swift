//
//  TilesViewModel.swift
//  tiles
//
//  Created by Anand, Chetan on 24/4/20.
//  Copyright © 2020 Chetan. All rights reserved.
//

import Foundation
class TilesViewModel {
    let service: NumberPairGeneratorService
    let items: Observable<[TileViewModel]> = Observable([])
    lazy var steps: Observable<String> = Observable("Steps: \(stepsCounter)")
    let alertMessage: Observable<String> = Observable("")

    private var stepsCounter = 0 {
        didSet {
            steps.value = "Steps: \(stepsCounter)"
        }
    }

    private var isGameover = false

    init(service: NumberPairGeneratorService) {
        self.service = service
    }

    func generateTiles() {
        service.generatePairs()
        items.value.removeAll()
        var tileViewModels = [TileViewModel]()
        for i in 0 ..< service.shuffledNumberPairs.count {
            let tile = Tile(id: i, value: service.shuffledNumberPairs[i])
            let tileViewModel = TileViewModel(tile: tile)
            tileViewModels.append(tileViewModel)
        }
        items.value = tileViewModels
    }

    func validate(tile: TileViewModel, afterDelay: TimeInterval, completion: @escaping (Bool, TileViewModel) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + afterDelay) { [weak self] in
            var isTilePairResolved = false
            guard let items = self?.items.value else {
                return
            }

            let pair = items.filter { $0.value == tile.value }

            if let firstMatch = pair.first,
                let secondMatch = pair.last,
                firstMatch.flipped, secondMatch.flipped {
                firstMatch.isResolved = true
                secondMatch.isResolved = true
                isTilePairResolved = true
            }
            self?.checkForGameover()
            completion(isTilePairResolved, tile)
        }
    }

    @discardableResult
    func checkForGameover() -> Bool {
        guard !isGameover else {
            return isGameover
        }

        defer {
            if isGameover {
                self.alertMessage.value = "You win this game by \(stepsCounter) steps!"
            }
        }
        isGameover = items.value.filter { $0.isResolved }.count == items.value.count
        return isGameover
    }

    func reset() {
        isGameover = false
        stepsCounter = 0
        generateTiles()
    }

    func incrementSteps() {
        stepsCounter += 1
    }
}
