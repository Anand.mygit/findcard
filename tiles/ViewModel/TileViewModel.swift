//
//  TileViewModel.swift
//  tiles
//
//  Created by Anand, Chetan on 25/4/20.
//  Copyright © 2020 Chetan. All rights reserved.
//

import Foundation
class TileViewModel {
    let id: Int
    let value: Int
    var flipped: Bool
    var isResolved: Bool = false

    init(tile: Tile) {
        id = tile.id
        value = tile.value
        flipped = tile.flipped
    }
}
