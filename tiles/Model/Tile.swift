//
//  Tile.swift
//  tiles
//
//  Created by Anand, Chetan on 24/4/20.
//  Copyright © 2020 Chetan. All rights reserved.
//

import Foundation
class Tile {
    let id: Int
    let value: Int
    var flipped: Bool = false

    init(id: Int, value: Int) {
        self.id = id
        self.value = value
    }
}
